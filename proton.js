const axios = require('axios');
const _ = require('underscore');

(async () => {
    const url = 'https://api.protonmail.ch/vpn/logicals';

    try {
        const page = await axios.get(url);

        const groupedData = _.groupBy(
            page.data.LogicalServers,
            'EntryCountry'
        );

        checkEmptyMandatoryValues(groupedData, 'ID');
        checkEmptyMandatoryValues(groupedData, 'EntryCountry');
        checkEmptyMandatoryValues(groupedData, 'ExitCountry');
        checkEmptyMandatoryValues(groupedData, 'Name');
        checkEmptyMandatoryValues(groupedData, 'Servers');
        checkEmptyMandatoryValues(groupedData, 'Domain');
        checkEmptyMandatoryValues(groupedData, 'Location');
        checkEmptyMandatoryValues(groupedData, 'City');
        checkEmptyMandatoryValues(groupedData, 'Region');
        checkLoadStatus(groupedData, 'Load');
        checkServerStatus(groupedData, 'Status');
        checkServerScore(groupedData, 'Score', 1.5);
        checkServerTier(groupedData, 'Tier');
        checkServerFeatures(groupedData, 'Features');
        checkMultipleServer(groupedData, 'Servers', 'EntryIP');
        checkMultipleServer(groupedData, 'Servers', 'ExitIP');
        checkMultipleServer(groupedData, 'Servers', 'Domain');
        checkMultipleServer(groupedData, 'Servers', 'ID');
        checkMultipleServer(groupedData, 'Servers', 'Status');
    } catch (e) {
        console.log(`I've crashed when getting server data ${e}`);
    }
})();

function checkEmptyMandatoryValues ($, fieldToCheck) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            //  checks for empty strings (""), null, undefined, false and the numbers 0 and NaN
            if (!$[item][i][fieldToCheck]) {
                console.log(`Please check the ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck}`);
            }
        }
    }
}

function checkLoadStatus ($, fieldToCheck) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            let color;

            if ($[item][i][fieldToCheck] >= 90) {
                color = 'red';
            } else if ($[item][i][fieldToCheck] >= 50) {
                color = 'yellow';
            } else if ($[item][i][fieldToCheck] >= 0) {
                color = 'green';
            } else {
                console.log(`Please check the load status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} as it's unclear`);
            }

            if (color){
                console.log(`Server load status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} is ${color}`);
            }
        }
    }
}

function checkServerStatus ($, fieldToCheck) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            let status;

            switch ($[item][i][fieldToCheck] ) {
                case 0:
                    status = 'offline';
                    break;
                case 1:
                    status = 'online';
                    break;
                default:
                    console.log(`Please check the server status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} as it's unclear`);
                    break;
            }

            if (status){
                console.log(`Server status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} is ${status}`);
            }
        }
    }
}

function checkServerScore ($, fieldToCheck, thresholdValue) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            if ($[item][i][fieldToCheck] <= thresholdValue) {
                console.log(`The server score for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} is excellent`);
            } else {
                console.log(`Please check the server score for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} as it's underperforming`);
            }
        }
    }
}

function checkServerTier ($, fieldToCheck) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            let tier;

            if ($[item][i][fieldToCheck] >= 2) {
                tier = 'plus server';
            } else if ($[item][i][fieldToCheck] > 0) {
                tier = 'regular';
            } else {
                console.log(`Please check the tier for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} as it's unclear`);
            }

            if (tier){
                console.log(`Server load status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} is ${tier}`);
            }
        }
    }
}

function checkServerFeatures ($, fieldToCheck) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);
        let coreServer, basicServer;

        for (let i = 0; i < serverGroupSize; i++) {
            let features;

            switch ($[item][i][fieldToCheck] ) {
                case 0:
                    features = 'basic server';
                    basicServer = true;
                    break;
                case 1:
                    features = 'core server';
                    coreServer = true;
                    break;
                case 2:
                    features = 'tor server';
                    break;
                case 4:
                    features = 'p2p server';
                    break;
                default:
                    console.log(`Please check the server features for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} as it's unclear`);
                    break;
            }

            if (features){
                console.log(`Server status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} is ${features}`);
            }
        }

        if (!(coreServer && basicServer)){
            console.log(`Server VPN for ${[item][0]} is not working`);
        }
    }
}

function checkMultipleServer ($, fieldToCheck, serverDetailedInfo) {
    for (const item of Object.keys($)) {
        const serverGroupSize = _.size($[item]);

        for (let i = 0; i < serverGroupSize; i++) {
            for (const entry of $[item][i][fieldToCheck]) {
                // This works for offline servers as 0 is checked with '!'
                if (!entry[serverDetailedInfo]){
                    console.log(`Server load status for ${$[item][i]['Name']} or ${$[item][i]['ID']} ${fieldToCheck} ${entry[serverDetailedInfo]}`);
                }
            }
        }
    }
}
