const cheerio = require('cheerio');
const axios = require('axios');

(async () => {

    const standard_count = 3;
    const regexp = /^[a-f0-9]{32}$/;
    for (let i = 1; i < 4001; i++) {
        const url = `http://username:password@host?page=${i}`;
        try {
            const page = await axios.get(url);
            const $ = cheerio.load(page.data);
            const h1 = $('h1');
            const a = $('a');
            const br = $('br');

            if ($('body > *').length !== standard_count) {
                console.log(`Take a look at this one ${url}`);
            }

            if (!regexp.test(h1.text())){
                console.log(`Take a look at this one ${url}`);
            }

            if (a.text() !== 'Next Page'){
                console.log(`Take a look at this one ${url}`);
            }

            if (br.length !== 1){
                console.log(`Take a look at this one ${url}`);
            }

        }
        catch (e) {
            console.log('Caught' + e);
            console.log(`Take a look at this error page ${url}`);
        }

        await new Promise(resolve => setTimeout(resolve, 500));
    }
})();